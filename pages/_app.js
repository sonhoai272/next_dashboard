import React from 'react';
import App from 'next/app';
import Router from 'next/router';
import MobileDetect from 'mobile-detect';
import NProgress from 'nprogress';

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

const LayoutDefault = (props) => (
    <div className="_app">
        {props.children}
    </div>
)

export default class DashboardApp extends App {

    static getInitialProps({ Component, ctx }) {
        const { req } = ctx;
        const pageProps = Component.getInitialProps ? 
            Component.getInitialProps(ctx) : {};
        const commonProps = {
            pageProps
        };

        if (req) {
            const md = new MobileDetect(req.headers['user-agent']);
            return {
                ...commonProps,
                isMobile: !!md.mobile()
            };
        }

        return {
            ...commonProps,
            isMobile: false
        }
    }

    render () {
        const { Component, pageProps, isMobile } = this.props;
        const LayoutComponent = Component?.layoutComponent || LayoutDefault;

        return (
            <LayoutComponent isMobile={ isMobile }>
                <Component { ...pageProps } />
            </LayoutComponent>
        );
    }
}
